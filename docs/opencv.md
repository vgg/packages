# OpenCV Emscripten build (without Pthreads)

The tar.gz archive was created by running the script [`build.sh`](../opencv/build.sh) present in the `opencv` folder
inside a container created from the emscripen docker image [`emscripten/emsdk:2.0.16`](https://hub.docker.com/layers/emscripten/emsdk/2.0.16/images/sha256-6ad6c2ae911ffe17059fc9a20848647e6b10823f5306e0275e29e79eb9f305b6?context=explore).

The opencv version was compiled without pthread support to make the resulting WASM work in Safari, since
[sharedarraybuffer is disabled in Safari](https://caniuse.com/?search=sharedarraybuffer) due
to Spectre vulnerabilities.

You can follow the steps below to reproduce the build.


## Requirements

- docker
- git
- `opencv/build.sh` from the repo

## Steps to reproduce
(Also in `opencv/repro.sh` - Can be run as)
```
bash opencv/repro.sh "4.5.5" # OPENCV_VERSION
```

```bash
#!/bin/bash

# Working directory is assumed to be a clone of this repo

OPENCV_VERSION="${1:-'4.5.5'}" # Change this to change version

# Clone opencv
git clone --depth 1 --branch "${OPENCV_VERSION}" https://github.com/opencv/opencv.git opencv_build

# Clone contrib modules
(cd opencv_build/ && git clone --depth 1 --branch "${OPENCV_VERSION}" https://github.com/opencv/opencv_contrib.git contrib)

# Copy the script to build
cp opencv/build.sh opencv_build/

# Pull emscripten docker image
docker pull emscripten/emsdk:2.0.16  # or use the sha - emscripten/emsdk@sha256:6ad6c2ae911ffe17059fc9a20848647e6b10823f5306e0275e29e79eb9f305b6

# Run the container
docker run --name embuild -dt -u $(id -u):$(id -g) -v ${PWD}/opencv_build:/code -w /code emscripten/emsdk:2.0.16 bash
```

This will launch the container and run it in the background with the right user directory permissions

Run the following command to install eigen from apt repository as root inside the container
```bash
#!/bin/bash

docker exec -u root embuild bash -c 'apt-get -yq update && apt-get install -y --no-install-recommends libeigen3-dev'
```

Once eigen is installed,
```bash
#!/bin/bash

docker exec embuild bash build.sh
```

You can change any opencv build flags you want in `build.sh`

You can now stop and optionally remove the container
```bash
#!/bin/bash

docker stop embuild
docker rm embuild
```

This will create an install directory which contains the minimum build in `opencv_build/install`

You can optionally create an archive for later use.
```bash
#!/bin/bash

tar -zcf "opencv_${OPENCV_VERSION}_nopthread_emscripten.tar.gz" -C opencv_build/install/ bin/ include/ lib/ share/
```
