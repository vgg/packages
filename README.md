# Packages

Repo to store all pre-built packages. Useful to save time when building libraries with dependencies that have longer compilation times.
## Available packages


### OpenCV Emscripten build

Pre-built opencv packages for use with WebAssembly based libraries.

- [Documentation](./docs/opencv.md)
- [Build script](./opencv/build.sh)
