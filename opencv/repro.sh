#!/bin/bash
set -euxo pipefail

# Working directory is assumed to be a clone of this repo
OPENCV_VERSION="${1:-4.5.5}" # OPENCV_VERSION passed as argument

# Clone opencv
git clone --depth 1 --branch "${OPENCV_VERSION}" https://github.com/opencv/opencv.git opencv_build

# Clone contrib modules
(cd opencv_build/ && git clone --depth 1 --branch "${OPENCV_VERSION}" https://github.com/opencv/opencv_contrib.git contrib)

# Copy the script to build
cp opencv/build.sh opencv_build/

# Pull emscripten docker image
docker pull emscripten/emsdk:2.0.16  # or use the sha - emscripten/emsdk@sha256:6ad6c2ae911ffe17059fc9a20848647e6b10823f5306e0275e29e79eb9f305b6

# Start the container in background
docker run --name embuild -dt -u $(id -u):$(id -g) -v ${PWD}/opencv_build:/code -w /code emscripten/emsdk:2.0.16 bash

# Install libeigen3-dev
docker exec -u root embuild bash -c 'apt-get -yq update && apt-get install -y --no-install-recommends libeigen3-dev'

# Compile
docker exec embuild bash build.sh

# Remove container
docker stop embuild
docker rm embuild

# Create a tar of install directory
tar -zcf "opencv_${OPENCV_VERSION}_nopthread_emscripten.tar.gz" -C opencv_build/install/ bin/ include/ lib/ share/